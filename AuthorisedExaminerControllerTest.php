<?php

namespace OrganisationApiTest\Controller;

use DvsaCommon\Dto\Organisation\AuthorisedExaminerListItemDto;
use DvsaCommon\Dto\Organisation\OrganisationDto;
use DvsaCommon\Utility\DtoHydrator;
use DvsaCommonApiTest\Controller\AbstractRestfulControllerTestCase;
use DvsaEntities\DqlBuilder\SearchParam\AuthorisedExaminerSearchParam;
use DvsaMotApi\Service\AuthorisedExaminerSearchService;
use OrganisationApi\Controller\AuthorisedExaminerController;
use OrganisationApi\Service\AuthorisedExaminerService;
use UserFacade\Role;
use Zend\Stdlib\Parameters;

/**
 * Tests for add, edit and view AuthorisedExaminer
 *
 * @package OrganisationApiTest\Controller
 */
class AuthorisedExaminerControllerTest extends AbstractRestfulControllerTestCase
{
    const AE_ID = 1;

    public function setUp()
    {
        $this->setController(new AuthorisedExaminerController());

        parent::setUp();
    }

    public function testGetListCanBeAccessed()
    {
        $this->mockValidAuthorization([Role::VEHICLE_EXAMINER]);

        $serviceReturn = [
            'search' => '',
            'data'   => [
                (new AuthorisedExaminerListItemDto())->setId(self::AE_ID),
            ]
        ];

        $mockAESearchService = $this->getMockSearchService();
        $this->setupMockForCalls($mockAESearchService, 'getAuthorisedExaminer', $serviceReturn);

        $mockAESearchService->expects($this->once())
            ->method('getSearchParams')
            ->will($this->returnValue(new AuthorisedExaminerSearchParam(null, $this->request)));

        $result = $this->controller->dispatch($this->request);

        $expectedSitesData = $serviceReturn;
        $expectedSitesData['data'] = (new DtoHydrator())->extract($expectedSitesData['data']);

        $this->assertResponseStatusAndResult(200, $this->getTestResponse($expectedSitesData), $result);
    }

    /**
     * Test method buildSearchParams
     *
     * @param array $queryParams List of parameters to set to request
     * @param array $expectResult
     *
     * @dataProvider dataProviderTestBuildSearchParams
     */
    public function testBuildSearchParams($queryParams, $expectResult)
    {
        //  --  set query parameters in request --
        if (!empty($queryParams)) {
            $this->request->setQuery(
                new Parameters($queryParams)
            );
        }

        //  --  mock service method --
        $mockAESearchService = $this->getMockSearchService();
        $mockAESearchService->expects($this->once())
            ->method('getSearchParams')
            ->will($this->returnValue(new AuthorisedExaminerSearchParam(null, $this->request)));

        //  --  mock checked protected method --
        $mockMethod = self::mockMethod(AuthorisedExaminerController::CLASS_PATH, 'buildSearchParams');
        $result = $mockMethod->invoke($this->getController())->toArray();

        //  --  check result    --
        $this->assertTrue(is_array($result) && !empty($result));

        foreach ($expectResult as $key => $value) {
            $this->assertEquals($value, $result[$key]);
        }
    }

    public function dataProviderTestBuildSearchParams()
    {
        return [
            [
                'queryParams' => null,
                'expect'      => [
                    AuthorisedExaminerSearchParam::QUERY_SEARCH_PARAMETER        => null,
                    AuthorisedExaminerSearchParam::QUERY_SEARCH_FILTER_PARAMETER => null,
                ],
            ],
            [
                'queryParams' => [
                    AuthorisedExaminerSearchParam::QUERY_SEARCH_PARAMETER        => 'A1234',
                    AuthorisedExaminerSearchParam::QUERY_SEARCH_FILTER_PARAMETER => 'FLTR123',
                ],
                'expect'      => [
                    AuthorisedExaminerSearchParam::QUERY_SEARCH_PARAMETER        => 'A1234',
                    AuthorisedExaminerSearchParam::QUERY_SEARCH_FILTER_PARAMETER => 'fltr123',
                ],
            ],
        ];
    }

    /**
     * Test method is accessible for call with valid parameters
     *
     * @param string $method        HTTP method
     * @param string $action        route action
     * @param string $serviceMethod mocked service method
     * @param array  $serviceReturn service method will return
     * @param array  $params        route parameters
     * @param array  $expectResult  expected method result
     *
     * @dataProvider dataProviderTestWithValidParam
     */
    public function testWithValidParam($method, $action, $serviceMethod, $serviceReturn, $params, $expectResult)
    {
        $this->mockValidAuthorization([Role::VEHICLE_EXAMINER]);

        $mockAEService = $this->getMockService();
        $this->setupMockForCalls($mockAEService, $serviceMethod, $serviceReturn);

        if ($method) {
            $this->request->setMethod($method);
        }

        if ($action) {
            $this->routeMatch->setParam('action', $action);
        }

        foreach ($params as $name => $value) {
            $this->routeMatch->setParam($name, $value);
        }

        $result = $this->controller->dispatch($this->request);

        $this->assertResponseStatusAndResult(self::HTTP_OK_CODE, $expectResult, $result);
    }

    public function dataProviderTestWithValidParam()
    {
        $getServiceResult = new OrganisationDto();
        $getServiceResult->setId(self::AE_ID);

        $getExpectResult = $this->getTestResponse((new DtoHydrator())->extract($getServiceResult));

        $postServiceResult = ['id' => self::AE_ID];
        $postExpectResult = $this->getTestResponse($postServiceResult);

        return [
            [
                'method'        => 'get',
                'action'        => null,
                'serviceMethod' => 'get',
                'serviceReturn' => $getServiceResult,
                'params'        => ['id' => self::AE_ID],
                'expectResult'  => $getExpectResult,
            ],
            ['put', null, 'update', $postServiceResult, ['id' => self::AE_ID, 'data' => []], $postExpectResult],
            ['post', null, 'create', $postServiceResult, ['data' => []], $postExpectResult],
        ];
    }

    public function testGetAuthorisedExaminerService()
    {
        $mockMethod = self::mockMethod(AuthorisedExaminerController::CLASS_PATH, 'getAuthorisedExaminerService');

        $mockService = $this->getMockService();

        $this->assertEquals($mockService, $mockMethod->invoke($this->getController()), '');
    }

    public function testGetAuthorisedExaminerSearchService()
    {
        $mockMethod = self::mockMethod(AuthorisedExaminerController::CLASS_PATH, 'getSearchService');

        $mockService = $this->getMockSearchService();

        $this->assertEquals($mockService, $mockMethod->invoke($this->getController()), '');
    }

    private function getMockService()
    {
        return $this->getMockServiceManagerClass(
            AuthorisedExaminerService::CLASS_PATH,
            AuthorisedExaminerService::CLASS_PATH
        );
    }

    private function getMockSearchService()
    {
        return $this->getMockServiceManagerClass(
            AuthorisedExaminerSearchService::CLASS_PATH,
            AuthorisedExaminerSearchService::CLASS_PATH
        );
    }

    protected function getTestResponse($data = [])
    {
        return [
            'data' => $data
        ];
    }
}
