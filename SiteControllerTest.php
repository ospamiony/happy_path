<?php

namespace OrganisationApiTest\Controller;

use DvsaCommonApiTest\Controller\AbstractRestfulControllerTestCase;
use DvsaCommonTest\Bootstrap;
use OrganisationApi\Controller\SiteController;
use OrganisationApi\Service\SiteService;

/**
 * Class SiteControllerTest
 *
 * @package OrganisationApiTest\Controller
 */
class SiteControllerTest extends AbstractRestfulControllerTestCase
{
    private $organisationId = 1;
    private $siteServiceMock;

    protected function setUp()
    {
        $this->controller = new SiteController();
        $this->siteServiceMock = $this->getSiteServiceMock();
        $this->setupServiceManager();

        parent::setUp();
    }

    public function testGetListWithOrganisationIdCanBeAccessed()
    {
        //given
        $this->routeMatch->setParam('organisationId', $this->organisationId);

        //when
        $result = $this->controller->getList();

        //then
        $this->assertInstanceOf("Zend\View\Model\JsonModel", $result);
    }

    private function getSiteServiceMock()
    {
        $siteServiceMock = \DvsaCommonTest\TestUtils\XMock::Of(SiteService::CLASS_PATH);

        $siteServiceMock->expects($this->once())
            ->method('getListForOrganisation')
            ->with($this->organisationId)
            ->will($this->returnValue([]));

        return $siteServiceMock;
    }

    private function setupServiceManager()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService(SiteService::CLASS_PATH, $this->siteServiceMock);

        $this->controller->setServiceLocator($serviceManager);
    }
}
