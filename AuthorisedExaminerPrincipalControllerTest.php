<?php

namespace OrganisationApiTest\Controller;

use DvsaCommonApiTest\Controller\AbstractRestfulControllerTestCase;
use DvsaCommonTest\Bootstrap;
use OrganisationApi\Controller\AuthorisedExaminerPrincipalController;
use OrganisationApi\Service\AuthorisedExaminerPrincipalService;

/**
 * Class AuthorisedExaminerPrincipalControllerTest
 *
 * @property-read AuthorisedExaminerPrincipalController $controller
 *
 * @package OrganisationApiTest\Controller
 */
class AuthorisedExaminerPrincipalControllerTest extends AbstractRestfulControllerTestCase
{
    private $authorisedExaminerId = 1;
    private $authorisedExaminerPrincipalId = 2;
    private $authorisedExaminerPrincipalService;


    public function testGetListWithOrganisationIdCanBeAccessed()
    {
        //given
        $this->routeMatch->setParam('authorisedExaminerId', $this->authorisedExaminerId);

        //when
        $result = $this->controller->getList();

        //then
        $this->assertInstanceOf("Zend\View\Model\JsonModel", $result);
    }

    public function testDeleteCanBeAccessed()
    {
        //given
        $this->routeMatch->setParam('authorisedExaminerId', $this->authorisedExaminerId);

        //whenΩ
        $result = $this->controller->delete($this->authorisedExaminerPrincipalId);

        //then
        $this->assertInstanceOf("Zend\View\Model\JsonModel", $result);
    }

    public function testCreateCanBeAccessed()
    {
        //given
        $this->routeMatch->setParam('authorisedExaminerId', $this->authorisedExaminerId);

        //whenΩ
        $result = $this->controller->create([]);

        //then
        $this->assertInstanceOf("Zend\View\Model\JsonModel", $result);
    }

    private function getAuthorisedExaminerPrincipalService()
    {
        $siteServiceMock = \DvsaCommonTest\TestUtils\XMock::Of(AuthorisedExaminerPrincipalService::CLASS_PATH);

        $siteServiceMock->expects($this->any())
            ->method('getForAuthorisedExaminer')
            ->with($this->authorisedExaminerId)
            ->will($this->returnValue([]));

        $siteServiceMock->expects($this->any())
            ->method('deletePrincipalForAuthorisedExaminer')
            ->with($this->authorisedExaminerId, $this->authorisedExaminerPrincipalId)
            ->will($this->returnValue([]));

        $siteServiceMock->expects($this->any())
            ->method('createForAuthorisedExaminer')
            ->with($this->authorisedExaminerId, [])
            ->will($this->returnValue([]));

        return $siteServiceMock;
    }

    private function setupServiceManager()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService(
            AuthorisedExaminerPrincipalService::CLASS_PATH, $this->authorisedExaminerPrincipalService
        );

        $this->controller->setServiceLocator($serviceManager);
    }
}
